import uuid
from enum import Enum

from tortoise import fields
from tortoise.models import Model

from entities import ARMOR, FEMALE, INPUT, MALE, OUTPUT, WEAPON


class User(Model):
    id = fields.IntField(pk=True)
    email = fields.CharField(max_length=255, unique=True)
    password = fields.CharField(max_length=255)
    balance = fields.DecimalField(max_digits=5, decimal_places=2, default=0)

    def __str__(self):
        return self.email


class Character(Model):
    class Genders(str, Enum):
        MALE = MALE
        FEMALE = FEMALE

    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    gender = fields.CharEnumField(Genders)
    user = fields.ForeignKeyField("models.User")


class BalanceHistory(Model):
    class Directions(str, Enum):
        INPUT = INPUT
        OUTPUT = OUTPUT

    id = fields.IntField(pk=True)
    user = fields.ForeignKeyField("models.User")
    direction = fields.CharEnumField(Directions)
    amount = fields.DecimalField(max_digits=5, decimal_places=2)
    created = fields.DatetimeField(auto_now_add=True)


class Thing(Model):
    class Types(str, Enum):
        WEAPON = WEAPON
        ARMOR = ARMOR

    id = fields.IntField(pk=True)
    title = fields.CharField(max_length=255)
    price = fields.DecimalField(max_digits=5, decimal_places=2)
    type = fields.CharEnumField(Types)

    def __str__(self):
        return self.title


class UserThing(Model):
    id = fields.IntField(pk=True)
    user = fields.ForeignKeyField("models.User")
    thing = fields.ForeignKeyField("models.Thing")
    put_on = fields.BooleanField(default=False)

    def __str__(self):
        return f"{self.user.email} - {self.thing.title}"


class Notification(Model):
    id = fields.IntField(pk=True)
    email = fields.CharField(max_length=255)
    subject = fields.CharField(max_length=255)
    message = fields.TextField()
    sent = fields.BooleanField(default=False)

    def __str__(self):
        return self.email


class Feeling(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    img = fields.TextField()

    def __str__(self):
        return self.name


class IFeel(Model):
    feeling = fields.ForeignKeyField("models.Feeling")
    user = fields.ForeignKeyField("models.User")
    created = fields.DateField()
    stress_level = fields.IntField()
    self_esteem = fields.IntField()
    note = fields.TextField()


class Project(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    identifier = fields.UUIDField(default=uuid.uuid4)


class Image(Model):
    id = fields.IntField(pk=True)
    project = fields.ForeignKeyField("models.Project")
    created = fields.DatetimeField(auto_now_add=True)
    updated = fields.DatetimeField(auto_now=True)
    original = fields.UUIDField()
    thumb_150_120 = fields.UUIDField(null=True)
    big_thumb_700_700 = fields.UUIDField(null=True)
    big_1920_1080 = fields.UUIDField(null=True)
    d2500 = fields.UUIDField(null=True)
    completed = fields.BooleanField(default=False)

    def __str__(self):
        return str(self.id)


class ProcessingCompleted(Model):
    id = fields.IntField(pk=True)
    image = fields.ForeignKeyField("models.Image")
    sent = fields.BooleanField(default=False)
