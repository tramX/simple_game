from contextlib import asynccontextmanager

from aiobotocore.session import get_session
from botocore.exceptions import ClientError

from settings import settings


class S3Client:
    def __init__(self):
        self.config = {
            "aws_access_key_id": settings.S3_ACCESS_KEY,
            "aws_secret_access_key": settings.S3_SECRET_KEY,
            "endpoint_url": settings.S3_ENDPOINT,
        }
        self.bucket_name = "gallery"
        self.session = get_session()

    @asynccontextmanager
    async def get_client(self):
        async with self.session.create_client("s3", **self.config) as client:
            yield client

    async def upload_file(
        self, object_name: str, object_file, bucket_name: str = "gallery"
    ) -> bool:
        self.bucket_name = bucket_name

        try:
            async with self.get_client() as client:
                await client.put_object(
                    Bucket=self.bucket_name,
                    Key=object_name,
                    Body=object_file.file,
                )
                return True
        except ClientError as e:
            print(f"Error uploading file: {e}")
            return False

    async def get_file(self, object_name: str, destination_path: str):
        try:
            async with self.get_client() as client:
                response = await client.get_object(
                    Bucket=self.bucket_name, Key=object_name
                )
                data = await response["Body"].read()
                print(f"File {object_name} downloaded")
        except ClientError as e:
            print(f"Error downloading file: {e}")


s3_client = S3Client()
