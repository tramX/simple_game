from apscheduler.schedulers.asyncio import AsyncIOScheduler

from dao import (
    get_not_completed_images,
    get_not_send_processing_completed,
    get_notifications,
    get_project_by_id,
)
from resize import resize_image
from settings import ws_projects
from utils import send_mail


async def send_notifications():
    notifications = await get_notifications()
    for notification in notifications:
        await send_mail(
            [notification.email], notification.subject, notification.message
        )
        notification.sent = True
        await notification.save(update_fields=["sent"])


async def task_resize():
    for image in await get_not_completed_images():
        await resize_image(image)


async def task_send_processing_completed_notification():
    for item in await get_not_send_processing_completed():
        project = await get_project_by_id(item.image.project_id)
        websocket = ws_projects.get(str(project.identifier))
        if websocket is not None:
            await websocket.send_text(f"Resize item {item.image.original} completed")
            item.sent = True
            await item.save(update_fields=["sent"])


scheduler = AsyncIOScheduler()
scheduler.add_job(send_notifications, "interval", seconds=1)
scheduler.add_job(task_resize, "interval", seconds=1)
scheduler.add_job(task_send_processing_completed_notification, "interval", seconds=1)
