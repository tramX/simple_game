class GetObjectError(Exception):
    def __init__(self, *args, **kwargs):
        self.message = args[0]
        self.code = kwargs.get("code") if kwargs.get("code") else None
        super().__init__()
