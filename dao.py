from decimal import Decimal
from typing import List, Optional

from models import (
    BalanceHistory,
    Character,
    Feeling,
    IFeel,
    Image,
    Notification,
    ProcessingCompleted,
    Project,
    Thing,
    User,
    UserThing,
)


async def get_user_by_email(email: str) -> Optional[User]:
    return await User.get_or_none(email=email)


async def get_user_by_id(user_id: int) -> Optional[User]:
    return await User.get_or_none(id=user_id)


async def create_user(email: str, password: str):
    await User.get_or_create(email=email, password=password)


async def get_user_character(user_id: int) -> Optional[Character]:
    return await Character.filter(user_id=user_id).first()


async def create_character(user_id: int, name: str, gender: str) -> Character:
    return await Character.create(user_id=user_id, name=name, gender=gender)


async def get_user_balance(user_id: int) -> Decimal:
    user: User = await User.get_or_none(id=user_id)

    if not user:
        return 0
    else:
        return user.balance


async def add_balance_history(user: User, direction: str, amount: Decimal) -> bool:
    await BalanceHistory.create(user=user, direction=direction, amount=amount)
    await create_notification(user.email, "Изменение баланса", "Был изменен баланс")
    return True


async def amount_in(user_id: int, amount: Decimal) -> bool:
    user: User = await User.get_or_none(id=user_id)

    if not user:
        return False
    else:
        user.balance += amount
        await user.save(update_fields=["balance"])
        await add_balance_history(user, BalanceHistory.Directions.INPUT, amount)
        return True


async def amount_out(user_id: int, amount: Decimal) -> bool:
    user: User = await User.get_or_none(id=user_id)

    if not user:
        return False
    else:
        user.balance += amount
        await user.save(update_fields=["balance"])
        await add_balance_history(user, BalanceHistory.Directions.OUTPUT, amount)
        return True


async def create_thing(title: str, price: Decimal, type: str) -> Thing:
    return await Thing.create(title=title, price=price, type=type)


async def get_thing_by_id(id: int) -> Optional[Thing]:
    return await Thing.filter(id=id).first()


async def get_things() -> List[Thing]:
    return await Thing.all()


async def create_user_thing(user: User, thing: Thing) -> UserThing:
    return await UserThing.create(user=user, thing=thing)


async def del_user_thing(thing: UserThing) -> bool:
    return await thing.delete()


async def get_user_things(user_id: int) -> List[UserThing]:
    return await UserThing.filter(user_id=user_id, put_on=False).prefetch_related(
        "thing"
    )


async def get_equipment(user_id: int) -> List[UserThing]:
    return await UserThing.filter(user_id=user_id, put_on=True).prefetch_related(
        "thing"
    )


async def get_user_thing_by_id(thing_id: int) -> Optional[UserThing]:
    return await UserThing.get_or_none(id=thing_id).prefetch_related("thing")


async def check_user_thing(thing: UserThing, user_id: int) -> bool:
    if thing.user_id == user_id:
        return True
    else:
        return False


async def put_on_thing(thing: UserThing) -> bool:
    thing.put_on = True
    await thing.save(update_fields=["put_on"])
    return True


async def take_off_thing(thing: UserThing) -> bool:
    thing.put_on = False
    await thing.save(update_fields=["put_on"])
    return True


async def get_user_balance_history(user: User) -> List[BalanceHistory]:
    return await BalanceHistory.filter(user=user)


async def hand_over_thing(thing: UserThing, user_from_id: int, user_to_id: int) -> bool:
    user = await get_user_by_id(user_to_id)
    await create_user_thing(user, thing.thing)
    await amount_out(user_from_id, thing.thing.price)
    await del_user_thing(thing)
    await create_notification(user.email, "Передача", "Вам передали вещ")
    return True


async def create_notification(email: str, subject: str, message: str):
    await Notification.create(email=email, subject=subject, message=message)


async def get_notifications() -> List[Notification]:
    return await Notification.filter(sent=False)


async def get_feelings() -> List[Feeling]:
    return await Feeling.all()


async def get_feeling_by_id(feeling_id) -> Optional[Feeling]:
    return await Feeling.get_or_none(id=feeling_id)


async def create_ifeeling(
    feeling, user, created, stress_level, self_esteem, note
) -> Optional[Feeling]:
    obj, status = await IFeel.get_or_create(
        feeling=feeling,
        user=user,
        created=created,
        stress_level=stress_level,
        self_esteem=self_esteem,
        note=note,
    )
    return obj


async def get_my_feeling(user, date_from, date_to) -> List[IFeel]:
    return await IFeel.filter(
        user=user, created__range=[date_from, date_to]
    ).prefetch_related("feeling")


async def create_project(name: str) -> Project:
    return await Project.create(name=name)


async def get_project_by_identifier(identifier: str) -> Optional[Project]:
    return await Project.get_or_none(identifier=identifier)


async def get_project_by_id(project_id: int) -> Optional[Project]:
    return await Project.get_or_none(id=project_id)


async def create_image(project: Project, original: str) -> Image:
    return await Image.create(project=project, original=original)


async def get_project_images(project: Project) -> List[Image]:
    return await Image.filter(project=project)


async def get_not_completed_images() -> List[Image]:
    return await Image.filter(completed=False)


async def get_not_send_processing_completed() -> List[ProcessingCompleted]:
    return await ProcessingCompleted.filter(sent=False).prefetch_related("image")
