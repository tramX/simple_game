from datetime import date, datetime
from decimal import Decimal
from typing import Optional

from pydantic import UUID4, BaseModel, EmailStr, SecretStr, field_validator

from models import Character


class UserScheme(BaseModel):
    email: EmailStr


class UserPasswordScheme(UserScheme):
    password: SecretStr


class Token(BaseModel):
    access_token: str


class DataToken(BaseModel):
    email: str


class UserDataScheme(UserScheme):
    id: int
    balance: Decimal

    @field_validator("balance")
    def convert_decimal(v):
        return Decimal("{:.{prec}f}".format(v, prec=2))


class CreateCharacterScheme(BaseModel):
    name: str
    gender: Character.Genders


class CharacterScheme(BaseModel):
    id: int


class TopUpBalanceScheme(BaseModel):
    amount: Decimal


class ResponseMessageScheme(BaseModel):
    status: bool
    message: str


class ThingScheme(BaseModel):
    id: int
    title: str
    price: Decimal
    type: str

    @field_validator("price")
    def convert_decimal(v):
        return Decimal("{:.{prec}f}".format(v, prec=2))


class BuyThingScheme(BaseModel):
    id: int


class UserThingScheme(BaseModel):
    id: int
    user_id: int
    title: str
    type: str


class PutOnThingScheme(BaseModel):
    id: int


class BalanceHistoryScheme(BaseModel):
    id: int
    user: int
    direction: str
    amount: Decimal
    created: datetime


class HandOverThingScheme(BaseModel):
    thing_id: int
    user_id: int


class FeelingScheme(BaseModel):
    id: int
    name: str
    img: str


class IFeelRequestScheme(BaseModel):
    feeling_id: int
    created: date
    stress_level: int
    self_esteem: int
    note: str

    @field_validator("stress_level")
    def stress_level(v):
        if v < 0:
            raise ValueError("Value error")
        return v

    @field_validator("self_esteem")
    def self_esteem(v):
        if v < 0:
            raise ValueError("Value error")
        return v


class GetMyFeelingRequestScheme(BaseModel):
    date_from: date
    date_to: date


class IFeelResponseScheme(BaseModel):
    name: str
    created: date
    stress_level: int
    self_esteem: int
    note: str


class NewProjectScheme(BaseModel):
    name: str


class ProjectScheme(NewProjectScheme):
    id: int
    identifier: UUID4


class ImageScheme(BaseModel):
    project_id: int
    created: datetime
    updated: datetime
    original: UUID4
    thumb_150_120: Optional[UUID4]
    big_thumb_700_700: Optional[UUID4]
    big_1920_1080: Optional[UUID4]
    d2500: Optional[UUID4]
    completed: bool


class RequestTextScheme(BaseModel):
    text: str
