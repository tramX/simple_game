import uuid

from models import Image, ProcessingCompleted


async def resize_image(image: Image):
    image.thumb_150_120 = uuid.uuid4()
    image.big_thumb_700_700 = uuid.uuid4()
    image.big_1920_1080 = uuid.uuid4()
    image.d2500 = uuid.uuid4()
    image.completed = True
    await image.save(
        update_fields=("thumb_150_120", "big_thumb_700_700", "d2500", "completed")
    )
    await ProcessingCompleted.create(image=image)
