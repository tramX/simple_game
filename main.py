import asyncio
from contextlib import asynccontextmanager

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordBearer
from tortoise.contrib.fastapi import register_tortoise
from tortoise.exceptions import ConfigurationError

from endpoints.auth import router as auth_router
from endpoints.feeling import router as feeling_router
from endpoints.gallery import router as gallery_router
from endpoints.natural_language_toolkit import router as nltk_router
from endpoints.user import router as user_router
from scheduler import scheduler
from settings import settings

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
import os

from tortoise import Tortoise


async def init():
    scheduler.start()
    db_url = settings.DB_URL
    try:
        await Tortoise.init(
            db_url=db_url,
            modules={"models": ["models"]},
        )
    except ConfigurationError:
        print(f"invalid database url {db_url}, please change it")
        os._exit(0)
    max_attempts = 60
    attempt = 0
    while attempt < max_attempts:
        try:
            await Tortoise.generate_schemas()
            print("sucessfully connected to database")
            return
        except OSError:
            attempt += 1
            print(
                f"waiting for postgresql to be ready, attempt {attempt}/{max_attempts}"
            )
            await asyncio.sleep(1)
    raise TimeoutError("failed to start postgresql")


@asynccontextmanager
async def lifespan(app: FastAPI):
    await init()
    yield
    os._exit(0)


app = FastAPI(lifespan=lifespan)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # can alter with time
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(auth_router, prefix="/api")
app.include_router(user_router, prefix="/api")
app.include_router(feeling_router, prefix="/api")
app.include_router(gallery_router, prefix="/api")
app.include_router(nltk_router, prefix="/api")


if __name__ == "__main__":
    uvicorn.run("main:app", reload=True)
