from typing import List, Tuple

from nltk import ne_chunk, pos_tag, sent_tokenize
from nltk.tokenize import word_tokenize


def get_tokens(text: str) -> List[str]:
    return word_tokenize(text)


def get_partial_marking(text: str) -> List[Tuple]:
    return pos_tag(text.split())


def get_named_entity_recognition(text: str) -> List[str]:
    result = []
    for sent in sent_tokenize(text):
        for chunk in ne_chunk(pos_tag(word_tokenize(sent))):
            if hasattr(chunk, "label"):
                result.append((chunk.label(), chunk[0][0]))
    return result
