from typing import List, Tuple

from fastapi import APIRouter

from endpoints.nltk_worker import (
    get_named_entity_recognition,
    get_partial_marking,
    get_tokens,
)
from scheme import RequestTextScheme

router = APIRouter(prefix="/nltk")


@router.post("/tokenization")
async def set_what_i_feel(
    request_data: RequestTextScheme,
) -> List[str]:
    return get_tokens(request_data.text)


@router.post("/partial-marking")
async def partial_marking(
    request_data: RequestTextScheme,
) -> List[Tuple]:
    return get_partial_marking(request_data.text)


@router.post("/named-entity-recognition")
async def named_entity_recognition(
    request_data: RequestTextScheme,
) -> List[Tuple]:
    return get_named_entity_recognition(request_data.text)
