from datetime import date
from typing import Annotated, List

from fastapi import APIRouter, Depends

from dao import create_ifeeling, get_feeling_by_id, get_feelings, get_my_feeling
from models import Feeling, User
from scheme import (
    FeelingScheme,
    GetMyFeelingRequestScheme,
    IFeelRequestScheme,
    IFeelResponseScheme,
    ResponseMessageScheme,
)
from security import get_current_user

router = APIRouter(prefix="/feeling")


@router.get("/feelings")
async def feelings() -> List[FeelingScheme]:
    return [
        FeelingScheme(id=item.id, name=item.name, img=item.img)
        for item in await get_feelings()
    ]


@router.post("/set-what-i-feel")
async def set_what_i_feel(
    request_data: IFeelRequestScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:
    feeling = await get_feeling_by_id(request_data.feeling_id)
    if not feeling:
        return ResponseMessageScheme(status=False, message="Not found")
    await create_ifeeling(
        feeling,
        current_user,
        request_data.created,
        request_data.stress_level,
        request_data.self_esteem,
        request_data.note,
    )
    return ResponseMessageScheme(status=True, message="Was created")


@router.get("/my-feelings")
async def my_feelings(
    date_from: date,
    date_to: date,
    current_user: Annotated[User, Depends(get_current_user)],
) -> List[IFeelResponseScheme]:
    records = await get_my_feeling(current_user, date_from, date_to)
    return [
        IFeelResponseScheme(
            name=record.feeling.name,
            created=record.created,
            note=record.note,
            stress_level=record.stress_level,
            self_esteem=record.self_esteem,
        )
        for record in records
    ]
