from fastapi import APIRouter, Response, status

from dao import create_user, get_user_by_email
from scheme import Token, UserPasswordScheme
from security import create_access_token, hash_pass, verify_password

router = APIRouter(prefix="/auth")


@router.post(
    "/register/",
)
async def register(request_data: UserPasswordScheme) -> Token:

    user = await get_user_by_email(request_data.email)

    if user is not None:
        return Response(status_code=status.HTTP_400_BAD_REQUEST)

    password_hash = hash_pass(request_data.password.get_secret_value())
    await create_user(request_data.email, password_hash)
    token = create_access_token(
        data={"email": request_data.email, "password": password_hash}
    )

    return Token(access_token=token)


@router.post(
    "/login",
)
async def login(request_data: UserPasswordScheme) -> Response:

    user = await get_user_by_email(request_data.email)

    if not user:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)
    else:
        verify_status = verify_password(
            request_data.password.get_secret_value(), user.password
        )
        if verify_status:
            token = create_access_token(
                data={"email": user.email, "password": user.password}
            )
            return Token(access_token=token)
    return Response(status_code=status.HTTP_401_UNAUTHORIZED)
