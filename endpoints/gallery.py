import uuid
from typing import List

from fastapi import APIRouter, HTTPException, UploadFile, WebSocket, status
from minio.error import InvalidResponseError
from pydantic import UUID4

from dao import (
    create_image,
    create_project,
    get_project_by_identifier,
    get_project_images,
)
from s3 import s3_client
from scheme import ImageScheme, NewProjectScheme, ProjectScheme, ResponseMessageScheme
from settings import ws_projects

router = APIRouter(prefix="/gallery", tags=["gallery"])

accepted_file_types = [
    "image/png",
    "image/jpeg",
    "image/jpg",
    "image/heic",
    "image/heif",
    "image/heics",
    "png",
    "jpeg",
    "jpg",
    "heic",
    "heif",
    "heics",
]


@router.post("/new-project")
async def new_project(request_data: NewProjectScheme) -> ProjectScheme:
    project = await create_project(request_data.name)
    return ProjectScheme(
        id=project.id, name=project.name, identifier=project.identifier
    )


@router.post("/images")
async def upload_image(identifier: UUID4, image: UploadFile) -> ResponseMessageScheme:
    if image.content_type not in accepted_file_types:
        raise HTTPException(
            status_code=status.HTTP_415_UNSUPPORTED_MEDIA_TYPE,
            detail="Unsupported file type",
        )

    project = await get_project_by_identifier(identifier)

    if not project:
        return ResponseMessageScheme(status=False, message="Project not found")
    else:
        object_name = str(uuid.uuid4())
        resp = await s3_client.upload_file(object_name, image)

        if resp:
            await create_image(project, object_name)
            return ResponseMessageScheme(status=True, message="Image uploaded")
        return ResponseMessageScheme(status=False, message="Server error")


@router.get("/projects/{id}/images")
async def get_image(id: str) -> List[ImageScheme]:
    project = await get_project_by_identifier(id)
    images = await get_project_images(project)

    return [
        ImageScheme(
            created=image.created,
            updated=image.updated,
            original=image.original,
            thumb_150_120=image.thumb_150_120,
            big_thumb_700_700=image.big_thumb_700_700,
            big_1920_1080=image.big_1920_1080,
            d2500=image.d2500,
            completed=image.completed,
            project_id=image.project_id,
        )
        for image in images
    ]


@router.websocket("/ws/{project_id}")
async def websocket_endpoint(project_id: str, websocket: WebSocket):
    await websocket.accept()

    ws_projects[project_id] = websocket

    while True:
        await websocket.receive_text()
    #    print(ws_projects)
    #    await websocket.send_text(f"Message text was: {data}")
