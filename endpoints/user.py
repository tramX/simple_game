from typing import Annotated, List

from fastapi import APIRouter, Depends

import dao
from dao import amount_in, create_character, get_user_balance, get_user_character
from models import User
from scheme import (
    BalanceHistoryScheme,
    BuyThingScheme,
    CharacterScheme,
    CreateCharacterScheme,
    HandOverThingScheme,
    PutOnThingScheme,
    ResponseMessageScheme,
    ThingScheme,
    TopUpBalanceScheme,
    UserDataScheme,
    UserThingScheme,
)
from security import get_current_user

router = APIRouter(prefix="/user")


@router.post("/create-character")
async def register(
    request_data: CreateCharacterScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> CharacterScheme:
    character = await get_user_character(current_user.id)
    if not character:
        character = await create_character(
            current_user.id, request_data.name, request_data.gender
        )
        return CharacterScheme(
            id=character.id, name=character.name, gender=character.gender
        )
    return CharacterScheme(
        id=character.id, name=character.name, gender=character.gender
    )


@router.get("/get-balance")
async def get_balance(
    current_user: Annotated[User, Depends(get_current_user)],
) -> UserDataScheme:
    balance = await get_user_balance(current_user.id)
    return UserDataScheme(id=current_user.id, email=current_user.email, balance=balance)


@router.post("/top-up-balance")
async def top_up_balance(
    request_data: TopUpBalanceScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:
    status = await amount_in(current_user.id, request_data.amount)
    message = "Top up balance success" if status else "Top up balance error"
    return ResponseMessageScheme(status=status, message=message)


@router.get("/get-things-in-shop")
async def get_things_in_shop() -> List[ThingScheme]:
    return [
        ThingScheme(id=thing.id, title=thing.title, price=thing.price, type=thing.type)
        for thing in await dao.get_things()
    ]


@router.post("/buy-thing")
async def buy_thing(
    request_data: BuyThingScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:

    thing = await dao.get_thing_by_id(request_data.id)

    if not thing:
        return ResponseMessageScheme(status=False, message="Thing not found")
    balance = await get_user_balance(current_user.id)

    if thing.price > balance:
        return ResponseMessageScheme(status=False, message="Insufficient funds")

    await dao.create_user_thing(current_user, thing)
    return ResponseMessageScheme(status=True, message="The item was purchased")


@router.get("/get-arsenal")
async def get_arsenal(
    current_user: Annotated[User, Depends(get_current_user)]
) -> List[UserThingScheme]:
    return [
        UserThingScheme(
            id=thing.id,
            user_id=current_user.id,
            title=thing.thing.title,
            type=thing.thing.type,
        )
        for thing in await dao.get_user_things(current_user.id)
    ]


@router.get("/get-equipment")
async def get_arsenal(
    current_user: Annotated[User, Depends(get_current_user)]
) -> List[UserThingScheme]:
    return [
        UserThingScheme(
            id=thing.id,
            user_id=current_user.id,
            title=thing.thing.title,
            type=thing.thing.type,
        )
        for thing in await dao.get_equipment(current_user.id)
    ]


@router.post("/put-on-thing")
async def put_on_thing(
    request_date: PutOnThingScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:

    user_thing = await dao.get_user_thing_by_id(request_date.id)

    if not user_thing:
        return ResponseMessageScheme(status=False, message="Thing not found")

    if await dao.check_user_thing(user_thing, current_user.id):
        await dao.put_on_thing(user_thing)
        return ResponseMessageScheme(
            status=True, message=f"Вы одели {user_thing.thing.title}"
        )
    else:
        return ResponseMessageScheme(status=False, message="Thing not found")


@router.post("/take-off-thing")
async def take_off_thing(
    request_date: PutOnThingScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:

    user_thing = await dao.get_user_thing_by_id(request_date.id)

    if not user_thing:
        return ResponseMessageScheme(status=False, message="Thing not found")

    if await dao.check_user_thing(user_thing, current_user.id):
        await dao.take_off_thing(user_thing)
        return ResponseMessageScheme(
            status=True, message=f"Вы одели {user_thing.thing.title}"
        )
    else:
        return ResponseMessageScheme(status=False, message="Thing not found")


@router.get("/get-balance-history")
async def get_balance_history(
    current_user: Annotated[User, Depends(get_current_user)]
) -> List[BalanceHistoryScheme]:
    return [
        BalanceHistoryScheme(
            id=rec.id,
            user=rec.user_id,
            created=rec.created,
            direction=rec.direction,
            amount=rec.amount,
        )
        for rec in await dao.get_user_balance_history(current_user)
    ]


@router.post("/hand-over-thing")
async def hand_over_thing(
    request_date: HandOverThingScheme,
    current_user: Annotated[User, Depends(get_current_user)],
) -> ResponseMessageScheme:
    user_thing = await dao.get_user_thing_by_id(request_date.thing_id)
    if (
        not user_thing
        or await dao.check_user_thing(user_thing, current_user.id) is False
    ):
        return ResponseMessageScheme(status=False, message="Thing not found")
    else:
        await dao.hand_over_thing(user_thing, current_user.id, request_date.user_id)
        return ResponseMessageScheme(status=False, message="Вещь передана")
