from datetime import date, datetime
from decimal import Decimal

import pytest
from asgi_lifespan import LifespanManager
from httpx import ASGITransport, AsyncClient

from dao import create_thing, create_user, get_user_by_email
from main import app
from models import Character, Feeling, Thing

email = "user1@gmail.com"
password = "1qazxsw2"


@pytest.fixture(scope="module")
def anyio_backend():
    return "asyncio"


@pytest.fixture(scope="module")
async def client():
    async with LifespanManager(app):
        transport = ASGITransport(app=app)
        async with AsyncClient(
            transport=transport, base_url="http://127.0.0.1:8000/api"
        ) as c:
            yield c


async def get_headers(client: AsyncClient):
    response = await client.post(
        "/auth/login", json={"email": email, "password": password}
    )
    return {"Authorization": "Bearer " + response.json()["access_token"]}


@pytest.mark.anyio
async def test_register(client: AsyncClient):
    response = await client.post(
        "/auth/register/", json={"email": email, "password": password}
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_login(client: AsyncClient):
    response = await client.post(
        "/auth/login", json={"email": email, "password": password}
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_create_character(client: AsyncClient):
    headers = await get_headers(client)

    response = await client.post(
        "/user/create-character",
        headers=headers,
        json={"name": "Deadpool", "gender": Character.Genders.MALE},
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_get_balance(client: AsyncClient):
    headers = await get_headers(client)

    response = await client.get("/user/get-balance", headers=headers)
    assert response.status_code == 200


@pytest.mark.anyio
async def test_top_up_balance(client: AsyncClient):
    headers = await get_headers(client)
    amount = 100
    response = await client.get("/user/get-balance", headers=headers)
    old_balance = Decimal(response.json().get("balance"))
    response = await client.post(
        "/user/top-up-balance", headers=headers, json={"amount": amount}
    )
    assert response.status_code == 200
    response = await client.get("/user/get-balance", headers=headers)
    new_balance = Decimal(response.json().get("balance"))

    assert (old_balance + amount) == new_balance


@pytest.mark.anyio
async def test_get_things_in_shop(client: AsyncClient):
    count = 10
    for i in range(0, count):
        await create_thing(f"Title {i}", 1, Thing.Types.ARMOR)

    response = await client.get("/user/get-things-in-shop")
    assert response.status_code == 200
    assert len(response.json()) == count


@pytest.mark.anyio
async def test_buy_thing(client: AsyncClient):
    thing = await create_thing("Title", 1, Thing.Types.ARMOR)
    headers = await get_headers(client)
    response = await client.post(
        "/user/buy-thing", headers=headers, json={"id": thing.id}
    )
    assert response.status_code == 200
    assert response.json().get("status") is True


@pytest.mark.anyio
async def test_arsenal(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.get("/user/get-arsenal", headers=headers)
    assert response.status_code == 200
    assert len(response.json()) == 1


@pytest.mark.anyio
async def test_put_on_thing(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.get("/user/get-arsenal", headers=headers)
    thing = response.json()[0]
    response = await client.post(
        "/user/put-on-thing", headers=headers, json={"id": thing.get("id")}
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_put_off_thing(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.get("/user/get-equipment", headers=headers)
    thing = response.json()[0]
    response = await client.post(
        "/user/take-off-thing", headers=headers, json={"id": thing.get("id")}
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_get_balance_history(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.get("/user/get-balance-history", headers=headers)
    assert response.status_code == 200


@pytest.mark.anyio
async def test_hand_over_thing(client: AsyncClient):
    await create_user("user2@gmail.com", "password")
    user = await get_user_by_email("user2@gmail.com")
    headers = await get_headers(client)
    response = await client.get("/user/get-arsenal", headers=headers)
    thing = response.json()[0]
    response = await client.post(
        "/user/hand-over-thing",
        headers=headers,
        json={"thing_id": thing.get("id"), "user_id": user.id},
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_feelings(client: AsyncClient):
    await Feeling.create(name="Радость", img="img")
    await Feeling.create(name="Страх", img="img")
    await Feeling.create(name="Бешенство", img="img")
    response = await client.get("/feeling/feelings")
    assert response.status_code == 200
    assert len(response.json()) == 3


@pytest.mark.anyio
async def test_set_what_i_feel(client: AsyncClient):
    headers = await get_headers(client)
    feeling = await Feeling.create(name="Сила", img="img")
    response = await client.post(
        "/feeling/set-what-i-feel",
        headers=headers,
        json={
            "feeling_id": feeling.id,
            "created": str(date.today()),
            "stress_level": 1,
            "self_esteem": 1,
            "note": "I am fine",
        },
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_my_feelings(client: AsyncClient):
    headers = await get_headers(client)
    current_date = str(date.today())
    response = await client.get(
        f"/feeling/my-feelings?date_from={current_date}&date_to={current_date}",
        headers=headers,
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_create_project(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.post(
        "/gallery/new-project",
        headers=headers,
        json={
            "name": "New project",
        },
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_upload_image(client: AsyncClient):
    files = {"image": open("orders.jpg", "rb")}
    headers = await get_headers(client)
    response = await client.post(
        "/gallery/new-project",
        headers=headers,
        json={
            "name": "New project",
        },
    )
    identifier = response.json().get("identifier")
    response = await client.post(
        f"/gallery/images?identifier={identifier}", headers=headers, files=files
    )
    assert response.status_code == 200


@pytest.mark.anyio
async def test_project_images(client: AsyncClient):
    headers = await get_headers(client)
    response = await client.post(
        "/gallery/new-project",
        headers=headers,
        json={
            "name": "New project",
        },
    )
    identifier = response.json().get("identifier")
    files = {"image": open("orders.jpg", "rb")}
    await client.post(
        f"/gallery/images?identifier={identifier}", headers=headers, files=files
    )
    response = await client.get(
        f"/gallery/projects/{identifier}/images", headers=headers
    )
    assert response.status_code == 200
    assert len(response.json()) > 0
